<?php
namespace Avris\Micrus\Social;

use Avris\Bag\BagHelper;
use Avris\Micrus\Bootstrap\EventDispatcherInterface;
use Avris\Micrus\Social\Event\MailSentEvent;
use Avris\Micrus\Mailer\Mail\AddressInterface;
use Avris\Micrus\Mailer\MailBuilder;
use Avris\Micrus\Mailer\Mailer;
use Avris\Bag\Bag;

class MailManager implements MailManagerInterface
{
    /** @var Mailer */
    protected $mailer;

    /** @var MailBuilder  */
    protected $mailBuilder;

    /** @var string */
    protected $dir;

    /** @var EventDispatcherInterface */
    private $dispatcher;

    public function __construct(
        Mailer $mailer,
        MailBuilder $mailBuilder,
        $dir,
        EventDispatcherInterface $dispatcher
    ) {
        $this->mailer = $mailer;
        $this->mailBuilder = $mailBuilder;
        $this->dir = $dir;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param string $templateName
     * @param AddressInterface[]|AddressInterface $receivers
     * @param array $vars
     * @param bool $spool
     * @return bool
     */
    public function send($templateName, $receivers, array $vars = [], $spool = false)
    {
        if (!$receivers) {
            return false;
        }

        $result = true;

        foreach (BagHelper::toArray($receivers) as $receiver) {
            $result &= $this->sendOne($templateName, $receiver, $vars, $spool);
        }

        return $result;
    }

    protected function sendOne($templateName, AddressInterface $receiver, array $vars = [], $spool = false)
    {
        if (!isset($vars['user'])) {
            $vars['user'] = $receiver;
        }
        $mail = $this->mailBuilder->build($templateName, $vars);
        $mail->addTo($receiver);
        $mail->embedImage('logo', $this->dir . '/logo.png');

        $event = new MailSentEvent($templateName, $receiver, $vars, $spool, $mail);
        $this->dispatcher->trigger($event);
        $mail = $event->getMail();
        $spool = $event->isSpool();

        return $mail && $this->mailer->send($mail, $spool);
    }
}
