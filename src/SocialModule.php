<?php
namespace Avris\Micrus\Social;

use Avris\Micrus\Bootstrap\Module;
use Avris\Micrus\Social\Avatar\AvatarGeneratorService;
use Avris\Micrus\Social\Avatar\AvatarService;
use Avris\Micrus\Social\Source as Source;
use Avris\Micrus\Tool\FileDownloader;
use Avris\Micrus\Localizator\Locale\YamlLocaleSet;

class SocialModule implements Module
{
    public function extendConfig($env, $rootDir)
    {
        return [
            'services' => [
                'socialManager' => [
                    'class' => SocialManager::class,
                    'params' => [
                        '@container',
                        '@userManager',
                        '@request.absoluteBase',
                        '@env',
                        '@config.social',
                        '#socialSource',
                    ],
                    'tags' => ['defaultParameters'],
                ],
                'socialViewsProvider' => [
                    'class' => SocialViewsProvider::class,
                    'tags' => ['templateDirs'],
                ],
                'socialLocaleSet' => [
                    'class' => YamlLocaleSet::class,
                    'params' => ['social', __DIR__ . '/Locale', 'en'],
                    'tags' => ['localeSet'],
                ],
                'userManager' => [
                    'class' => UserManager::class,
                    'params' => [
                        '@orm.entityManager',
                        '@securityManager',
                        '@crypt',
                        '@request',
                        '@flashBag',
                        '@mailManager',
                        '@router',
                        '@dispatcher',
                        '@config.social.?afterLogin'
                    ]
                ],
                'mailManager' => [
                    'class' => MailManager::class,
                    'params' => [
                        '@mailer',
                        '@mailBuilder',
                        '{@rootDir}/app/Asset/mail/gfx/',
                        '@dispatcher',
                    ],
                ],
                'socialLoginPendingRequestListener' => [
                    'class' => PendingRequestListener::class,
                    'params' => ['@config.?social.?pendingAllowed'],
                    'events' => ['request'],
                ],
                'fileDownloader' => [
                    'class' => FileDownloader::class,
                ],
                'avatar' => [
                    'class' => AvatarService::class,
                    'params' => ['@socialManager', '@fileDownloader', '@avatarGenerator', 36],
                ],
                'avatarGenerator' => [
                    'class' => AvatarGeneratorService::class,
                    'params' => [__DIR__ . '/Avatar/Ubuntu-B.ttf'],
                ],
                'socialSourceFacebook' => [
                    'class' => Source\Facebook\FacebookSource::class,
                    'params' => ['@config.parameters.?social.?facebook'],
                    'tags' => ['socialSource'],
                ],
                'socialSourceGoogle' => [
                    'class' => Source\Google\GoogleSource::class,
                    'params' => ['@config.parameters.?social.?google'],
                    'tags' => ['socialSource'],
                ],
                'socialSourceTwitter' => [
                    'class' => Source\Twitter\TwitterSource::class,
                    'params' => ['@config.parameters.?social.?twitter'],
                    'tags' => ['socialSource'],
                ],
                'socialSourceGithub' => [
                    'class' => Source\Github\GithubSource::class,
                    'params' => ['@config.parameters.?social.?github'],
                    'tags' => ['socialSource'],
                ],
                'socialSourceGitlab' => [
                    'class' => Source\Gitlab\GitlabSource::class,
                    'params' => ['@config.parameters.?social.?gitlab'],
                    'tags' => ['socialSource'],
                ],
                'socialSourceLinkedin' => [
                    'class' => Source\Linkedin\LinkedinSource::class,
                    'params' => ['@config.parameters.?social.?linkedin'],
                    'tags' => ['socialSource'],
                ],
                'socialSourceMicrosoft' => [
                    'class' => Source\Microsoft\MicrosoftSource::class,
                    'params' => ['@config.parameters.?social.?microsoft'],
                    'tags' => ['socialSource'],
                ],
                'socialSourceYahoo' => [
                    'class' => Source\Yahoo\YahooSource::class,
                    'params' => ['@config.parameters.?social.?yahoo'],
                    'tags' => ['socialSource'],
                ],
            ],
            'routing' => [
                'socialLogin' =>       'GET  /login/{source}        -> @socialManager/login',
                'socialLoginUpdate' => 'POST /login/{source}/update -> @socialManager/update',
                'socialLoginRevoke' => 'POST /login/{source}/revoke -> @socialManager/revoke',
            ],
        ];
    }
}
