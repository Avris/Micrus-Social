<?php
/**
 * Created by PhpStorm.
 * User: andrzej.prusinowski
 * Date: 20/01/17
 * Time: 14:38
 */
namespace Avris\Micrus\Social;

use Avris\Micrus\Mailer\Mail\AddressInterface;

interface MailManagerInterface
{
    /**
     * @param string $templateName
     * @param AddressInterface[]|AddressInterface $receivers
     * @param array $vars
     * @param bool $spool
     * @return bool
     */
    public function send($templateName, $receivers, array $vars = [], $spool = false);
}