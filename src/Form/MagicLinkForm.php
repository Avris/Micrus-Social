<?php
namespace Avris\Micrus\Social\Form;

use App\Model\User;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget as Widget;

class MagicLinkForm extends BaseUserForm
{
    /** @var User */
    protected $user;

    protected function configure()
    {
        $this
            ->add(
                'identifier',
                $this->usesUsername() ? Widget\Text::class : Widget\Email::class,
                [
                    'helper' => l('user.magicLink.description')
                ],
                new Assert\NotBlank()
            )
        ;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user ?: $this->user = $this->container->get('userProvider')->getUser($this->object->identifier);
    }

    public function getName()
    {
        return 'MagicLink';
    }
}
