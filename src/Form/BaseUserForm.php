<?php
namespace Avris\Micrus\Social\Form;

use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Form;
use Avris\Micrus\Forms\Widget as Widget;

abstract class BaseUserForm extends Form
{
    protected function addWidgetUsername()
    {
        if (!$this->usesUsername()) {
            return $this;
        }

        return $this->add('username', Widget\Text::class, [], [
            new Assert\NotBlank(),
            new Assert\Regexp('^[A-Za-z0-9_]+$', l('validator.usernameRegex')),
            new Assert\MinLength(5),
            new Assert\MaxLength(25),
            new Assert\Unique(
                $this->container->get('orm'), $this->object, 'User', 'username',
                l('validator.usernameTaken')
            ),
        ]);
    }

    protected function addWidgetEmail($visible = true)
    {
        if (!$visible) {
            return $this;
        }

        return $this->add('email', Widget\Email::class, [], [
            new Assert\NotBlank(),
            new Assert\Unique(
                $this->container->get('orm'), $this->object, 'User', 'email',
                l('validator.emailTaken')
            ),
        ]);
    }

    protected function addWidgetPasswords($required)
    {
        $asserts = [ new Assert\MinLength(5) ];

        if ($required) {
            $asserts[] = new Assert\NotBlank();
        }

        $this->add('password', Widget\Password::class, [], $asserts);
        $this->add('passwordRepeat', Widget\Password::class, [], $asserts);

        return $this;
    }

    protected function addWidgetTerms()
    {
        $terms = $this->container->get('config.social.?terms');

        if (!$terms) {
            return $this;
        }

        /** @var RouterInterface $router */
        $router = $this->container->get('router');

        return $this->add('terms', Widget\Checkbox::class, [
            'label' => '',
            'sublabel' => l('user.terms.checkbox', ['url' => $router->getUrl($terms)])
        ], new Assert\NotBlank);
    }

    public function doPasswordsMatch($user)
    {
        return $user->password === $user->passwordRepeat
            ? true
            : l('validator.doPasswordsMatch');
    }

    public function getPassword()
    {
        return $this->object->password;
    }

    protected function usesUsername()
    {
        return (bool) $this->container->get('config.social.?useUsername', false);
    }
}