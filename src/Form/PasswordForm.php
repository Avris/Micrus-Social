<?php
namespace Avris\Micrus\Social\Form;

use Avris\Micrus\Forms\Widget as Widget;

class PasswordForm extends BaseUserForm
{
    protected function configure()
    {
        $this
            ->add('doPasswordsMatch', Widget\ObjectValidator::class)
            ->addWidgetPasswords(true)
        ;
    }

    public function getName()
    {
        return 'password';
    }
}