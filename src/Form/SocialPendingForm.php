<?php
namespace Avris\Micrus\Social\Form;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget as Widget;

class SocialPendingForm extends AccountForm
{
    protected function configure() {
        $this
            ->addWidgetUsername()
            ->addWidgetEmail(!$this->options->get('emailConfirmed'))
            ->add('doPasswordsMatch', Widget\ObjectValidator::class)
            ->addWidgetPasswords(false)
            ->addWidgetTerms()
        ;
    }

    public function getName()
    {
        return 'Register';
    }
}