<?php
namespace Avris\Micrus\Social\Form;

class AccountForm extends BaseUserForm
{
    protected function configure()
    {
        $this
            ->addWidgetEmail()
        ;
    }

    public function getName()
    {
        return 'account';
    }
}