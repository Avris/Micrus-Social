<?php
namespace Avris\Micrus\Social\Form;

use App\Model\User;
use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget as Widget;

class LoginForm extends BaseUserForm
{
    /** @var User */
    protected $user;

    protected function configure()
    {
        $this
            ->add('isActive', Widget\ObjectValidator::class)
            ->add('hasConfirmedEmail', Widget\ObjectValidator::class)
            ->add(
                'identifier',
                $this->usesUsername() ? Widget\Text::class : Widget\Email::class,
                [],
                new Assert\NotBlank()
            )
            ->add('password', Widget\Password::class, [], [
                new Assert\NotBlank(),
                new Assert\CorrectPassword(
                    [$this, 'getUser'],
                    $this->container->get('crypt')
                )
            ])
        ;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user ?: $this->user = $this->container->get('userProvider')->getUser($this->object->identifier);
    }

    public function hasConfirmedEmail()
    {
        if (!$this->getUser()) {
            return true;
        }

        return $this->getUser()->getEmail() !== null ? true : 'social.login.emailNotConfirmed';
    }

    public function isActive()
    {
        if (!$this->getUser()) {
            return true;
        }

        return $this->getUser()->isActive() ? true : 'social.login.inactive';
    }

    public function getName()
    {
        return 'Login';
    }
}
