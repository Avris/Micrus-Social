<?php
namespace Avris\Micrus\Social\Form;

use Avris\Micrus\Forms\Assert as Assert;
use Avris\Micrus\Forms\Widget as Widget;

class RegisterForm extends BaseUserForm
{
    protected function configure()
    {
        $this
            ->addWidgetUsername()
            ->addWidgetEmail()
            ->add('doPasswordsMatch', Widget\ObjectValidator::class)
            ->addWidgetPasswords(true)
            ->addWidgetTerms()
        ;
    }

    public function getName()
    {
        return 'Register';
    }
}
