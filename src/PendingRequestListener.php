<?php
namespace Avris\Micrus\Social;

use Avris\Bag\BagHelper;
use Avris\Micrus\Controller\Http\RedirectResponse;
use Avris\Micrus\Controller\Http\RequestEvent;
use Avris\Micrus\Controller\Http\SessionBag;

class PendingRequestListener
{
    /** @var string[] */
    private $allowedRoutes;

    public function __construct($allowedRoutes = null)
    {
        $this->allowedRoutes = array_merge(
            [UserManager::SOCIAL_LOGIN_PENDING, 'changeLocale', 'userConfirmEmail'],
            BagHelper::toArray($allowedRoutes ?: [])
        );
    }

    public function onRequest(RequestEvent $event)
    {
        /** @var SessionBag $session */
        $session = $event->getContainer()->get('request.session');
        if ($session->has(UserManager::SOCIAL_LOGIN_PENDING)) {
            $currentRoute = $event->getRequest()->getRouteMatch()->getRoute()->getName();
            if (!in_array($currentRoute, $this->allowedRoutes)) {
                $url = $event->getContainer()->get('router')->getUrl(UserManager::SOCIAL_LOGIN_PENDING);
                $event->stopPropagation()->setResponse(new RedirectResponse($url));
            }
        }
    }
}
