<?php
namespace Avris\Micrus\Social\Source\Yahoo;

use Avris\Micrus\Social\Source\Account;

class YahooAccount extends Account
{
    protected static $map = [
        'id' => 'guid',
        'name' => 'nickname',
        'avatar' => 'imageUrl',
        'link' => 'profileUrl',
    ];

    public function getAvatar($size = 36)
    {
        return str_replace('48.png', $size . '.png', $this->avatar);
    }
}
