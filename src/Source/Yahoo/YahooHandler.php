<?php
namespace Avris\Micrus\Social\Source\Yahoo;

use Avris\Micrus\Controller\Http\Request;
use Avris\Micrus\Exception\UnauthorisedException;
use Avris\Micrus\Social\Source\Handler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class YahooHandler extends Handler
{
    const AUTHORIZE_URL = 'https://api.login.yahoo.com/oauth2/request_auth';
    const TOKEN_URL = 'https://api.login.yahoo.com/oauth2/get_token';
    const API_URL_BASE = 'https://social.yahooapis.com/v1';

    const SESSION_STATE = 'yahoo_state';

    /** @var Client */
    protected $client;

    /** @var string */
    protected $clientId;

    /** @var string */
    protected $secret;

    public function __construct($access)
    {
        $this->client = new Client();
        $this->clientId = $access['clientId'];
        $this->secret = $access['secret'];
    }

    public function login(Request $request, $redirectUrl)
    {
        if ($request->getQuery('error')) {
            throw new UnauthorisedException(sprintf('Access denied by user, error: %s', $request->getQuery('error')));
        }

        if (!$request->getQuery('code')) {
            $state = hash('sha256', microtime(true) . rand() . $request->getIp());

            $request->getSession()->set(self::SESSION_STATE, $state);

            $params = [
                'client_id' => $this->clientId,
                'redirect_uri' => $redirectUrl,
                'state' => $state,
                'response_type' => 'code',
            ];

            return self::AUTHORIZE_URL . '?' . http_build_query($params);
        }

        $queryState = $request->getQuery('state');
        $sessionState = $request->getSession(self::SESSION_STATE);
        if (!$queryState || $queryState !== $sessionState) {
            throw new UnauthorisedException('Invalid state');
        }

        $response = $this->client->request('POST', self::TOKEN_URL, [
            'form_params' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->secret,
                'redirect_uri' => $redirectUrl,
                'state' => $sessionState,
                'code' => $request->getQuery('code'),
                'grant_type' => 'authorization_code',
            ],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $response = json_decode($response->getBody(), true);

        if (!isset($response['access_token'])) {
            throw new UnauthorisedException('Invalid access token');
        }

        return $this->loadUserData($response['refresh_token']);
    }

    public function update(array $payload)
    {
        return $this->loadUserData($this->refreshToken($payload['refreshToken']));
    }

    public function revoke(array $payload)
    {
        return true;
    }

    /**
     * @param string $refreshToken
     * @return array
     */
    protected function loadUserData($refreshToken)
    {
        $accessToken = $this->refreshToken($refreshToken);

        $response = $this->client->request('GET', self::API_URL_BASE . '/me/guid?format=json', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);
        $response = json_decode($response->getBody(), true);
        $guid = $response['guid']['value'];

        $response = $this->client->request(
            'GET',
            self::API_URL_BASE . '/user/' .$guid . '/profile/tinyusercard?format=json',
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $accessToken,
                ],
            ]
        );
        $response = json_decode($response->getBody(), true);

        $userData = [
            'id' => $guid,
            'nickname' => $response['profile']['nickname'],
            'profileUrl' => $response['profile']['profileUrl'],
            'imageUrl' => $response['profile']['image']['imageUrl'],
            'refreshToken' => $refreshToken,
        ];

        return $userData;
    }

    protected function refreshToken($refreshToken)
    {
        $response = $this->client->request('POST', self::TOKEN_URL, [
            'form_params' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->secret,
                'grant_type' => 'refresh_token',
                'refresh_token' => $refreshToken,
            ],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $response = json_decode($response->getBody(), true);

        return $response['access_token'];
    }
}