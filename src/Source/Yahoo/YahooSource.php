<?php
namespace Avris\Micrus\Social\Source\Yahoo;

use Avris\Micrus\Social\Source\SocialSource;
use Avris\Micrus\Tool\Config\ParametersProvider;

class YahooSource extends SocialSource
{
    protected static $name = 'yahoo';
    protected static $icon = 'fa fa-yahoo';
    protected static $handlerClass = YahooHandler::class;
    protected static $accountClass = YahooAccount::class;

    public function getParameters()
    {
        return [
            'clientId' => 'YAHOO_CLIENT_ID',
            'secret' => 'YAHOO_CLIENT_SECRET',
        ];
    }
}
