<?php
namespace Avris\Micrus\Social\Source\Twitter;

use Abraham\TwitterOAuth\TwitterOAuth;
use Avris\Micrus\Controller\Http\Request;
use Avris\Micrus\Exception\UnauthorisedException;
use Avris\Micrus\Social\Source\Handler;

class TwitterHandler extends Handler
{
    /** @var string */
    protected $key;

    /** @var string */
    protected $secret;

    public function __construct($access)
    {
        $this->key = $access['key'];
        $this->secret = $access['secret'];
    }

    public function login(Request $request, $redirectUrl)
    {
        if ($request->getQuery('denied')) {
            throw new UnauthorisedException('Access denied by user');
        }

        $oauthToken = $request->getQuery('oauth_token');
        $oauthVerifier = $request->getQuery('oauth_verifier');
        $session = $request->getSession();

        if (!$oauthToken || !$oauthVerifier) {
            $connection = $this->buildConnection();
            $requestToken = $connection->oauth('oauth/request_token', ['oauth_callback' => $redirectUrl]);

            if ($connection->getLastHttpCode() !== 200) {
                throw new \RuntimeException('Could not connect to Twitter.');
            }

            $session->set('oauth_token', $requestToken['oauth_token']);
            $session->set('oauth_token_secret', $requestToken['oauth_token_secret']);

            return $connection->url('oauth/authorize', ['oauth_token' => $requestToken['oauth_token']]);
        }

        $requestToken = [
            'oauth_token' => $session->get('oauth_token'),
            'oauth_token_secret' => $session->get('oauth_token_secret'),
        ];

        if ($oauthToken && $oauthToken !== $session->get('oauth_token')) {
            throw new UnauthorisedException('Invalid access token.');
        }

        $accessToken = $this->buildConnection($requestToken)
            ->oauth('oauth/access_token', ['oauth_verifier' => $oauthVerifier]);

        return $this->loadUserData($accessToken);
    }

    public function update(array $payload)
    {
        return $this->loadUserData($payload);
    }

    public function revoke(array $payload)
    {
        return true;
    }

    protected function buildConnection($token = null)
    {
        return $token
            ? new TwitterOAuth($this->key, $this->secret, $token['oauth_token'], $token['oauth_token_secret'])
            : new TwitterOAuth($this->key, $this->secret);
    }

    /**
     * @param string[] $accessToken
     * @return array
     */
    protected function loadUserData($accessToken)
    {
        $user = $this->buildConnection($accessToken)
            ->get('account/verify_credentials');

        return [
            'id' => $user->id,
            'name' => $user->screen_name,
            'subname' => $user->name,
            'avatar' => $user->profile_image_url_https,
            'oauth_token' => $accessToken['oauth_token'],
            'oauth_token_secret' => $accessToken['oauth_token_secret'],
        ];
    }
}