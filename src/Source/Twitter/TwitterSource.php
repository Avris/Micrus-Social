<?php
namespace Avris\Micrus\Social\Source\Twitter;

use Avris\Micrus\Social\Source\SocialSource;
use Avris\Micrus\Tool\Config\ParametersProvider;

class TwitterSource extends SocialSource
{
    protected static $name = 'twitter';
    protected static $icon = 'fa fa-twitter-square';
    protected static $handlerClass = TwitterHandler::class;
    protected static $accountClass = TwitterAccount::class;

    public function getParameters()
    {
        return [
            'key' => 'TWITTER_KEY',
            'secret' => 'TWITTER_SECRET',
        ];
    }
}
