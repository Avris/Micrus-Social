<?php
namespace Avris\Micrus\Social\Source\Twitter;

use Avris\Micrus\Social\Source\Account;

class TwitterAccount extends Account
{
    protected static $map = [
        'id' => 'id',
        'name' => 'subname',
        'subname' => 'name',
        'email' => 'email',
        'link' => 'link',
        'avatar' => 'avatar',
    ];

    public function getLink()
    {
        return 'https://twitter.com/' . $this->subname;
    }

    public function getAvatar($size = 36)
    {
        return str_replace('_normal', $this->getAvatarSizeName($size), $this->avatar);
    }

    protected function getAvatarSizeName($size)
    {
        if ($size <= 24) {
            return '_mini';
        } elseif ($size <= 48) {
            return '_normal';
        } elseif ($size <= 73) {
            return '_bigger';
        }

        return '';
    }

    public function getSubName()
    {
        return '@' . $this->subname;
    }
}