<?php
namespace Avris\Micrus\Social\Source\Microsoft;

use Avris\Micrus\Social\Source\SocialSource;
use Avris\Micrus\Tool\Config\ParametersProvider;

class MicrosoftSource extends SocialSource
{
    protected static $name = 'microsoft';
    protected static $icon = 'fa fa-windows';
    protected static $handlerClass = MicrosoftHandler::class;
    protected static $accountClass = MicrosoftAccount::class;

    public function getParameters()
    {
        return [
            'clientId' => 'MICROSOFT_CLIENT_ID',
            'clientSecret' => 'MICROSOFT_CLIENT_SECRET',
        ];
    }
}
