<?php
namespace Avris\Micrus\Social\Source\Microsoft;

use Avris\Micrus\Controller\Http\Request;
use Avris\Micrus\Exception\UnauthorisedException;
use Avris\Micrus\Social\Source\Handler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class MicrosoftHandler extends Handler
{
    const AUTHORIZE_URL = 'https://login.live.com/oauth20_authorize.srf';
    const TOKEN_URL = 'https://login.live.com/oauth20_token.srf';
    const API_URL_BASE = 'https://apis.live.net/v5.0';

    const SESSION_STATE = 'microsoft_state';

    /** @var Client */
    protected $client;

    /** @var string */
    protected $clientId;

    /** @var string */
    protected $secret;

    public function __construct($access)
    {
        $this->client = new Client();
        $this->clientId = $access['clientId'];
        $this->secret = $access['clientSecret'];
    }

    public function login(Request $request, $redirectUrl)
    {
        if ($request->getQuery('error')) {
            throw new UnauthorisedException(sprintf('Access denied by user, error: %s', $request->getQuery('error')));
        }

        if (!$request->getQuery('code')) {
            $state = hash('sha256', microtime(true) . rand() . $request->getIp());

            $request->getSession()->set(self::SESSION_STATE, $state);

            $params = [
                'client_id' => $this->clientId,
                'scope' => 'wl.basic,wl.emails',
                'redirect_uri' => $redirectUrl,
                'response_type' => 'code',
                'state' => $state,
            ];

            return self::AUTHORIZE_URL . '?' . http_build_query($params);
        }

        $queryState = $request->getQuery('state');
        $sessionState = $request->getSession(self::SESSION_STATE);
        if (!$queryState || $queryState !== $sessionState) {
            throw new UnauthorisedException('Invalid state');
        }

        $response = $this->client->request('POST', self::TOKEN_URL, [
            'form_params' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->secret,
                'redirect_uri' => $redirectUrl,
                'state' => $sessionState,
                'code' => $request->getQuery('code'),
                'grant_type' => 'authorization_code',
            ],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $response = json_decode($response->getBody(), true);

        if (!isset($response['access_token'])) {
            throw new UnauthorisedException('Invalid access token');
        }

        return $this->loadUserData($response['access_token']);
    }

    /**
     * @param string $accessToken
     * @return array
     */
    protected function loadUserData($accessToken)
    {
        $response = $this->client->request('GET', self::API_URL_BASE . '/me?access_token=' . $accessToken);
        $response = json_decode($response->getBody(), true);

        $interestingKeys = ['id', 'name', 'link'];
        $userData = array_intersect_key($response, array_flip($interestingKeys));
        $userData['email'] = $response['emails']['preferred'];
        $userData['accessToken'] = $accessToken;

        $response = $this->client->request('GET', self::API_URL_BASE . '/me/picture?access_token=' . $accessToken);
        $userData['avatar'] = 'data:image/jpg;base64,' . base64_encode((string) $response->getBody());

        return $userData;
    }

    public function revoke(array $payload)
    {
        return true;
    }

    public function update(array $payload)
    {
        return $this->loadUserData($payload['accessToken']);
    }
}