<?php
namespace Avris\Micrus\Social\Source\Gitlab;

use Avris\Micrus\Social\Source\Account;

class GitlabAccount extends Account
{
    protected static $map = [
        'id' => 'id',
        'name' => 'name',
        'subname' => 'username',
        'email' => 'email',
        'avatar' => 'avatar_url',
        'link' => 'web_url',
    ];

    public function getAvatar($size = 36)
    {
        return str_replace('s=80', 's='.$size, $this->avatar);
    }
}
