<?php
namespace Avris\Micrus\Social\Source\Gitlab;

use Avris\Micrus\Social\Source\SocialSource;
use Avris\Micrus\Tool\Config\ParametersProvider;

class GitlabSource extends SocialSource
{
    protected static $name = 'gitlab';
    protected static $icon = 'fa fa-gitlab';
    protected static $handlerClass = GitlabHandler::class;
    protected static $accountClass = GitlabAccount::class;

    public function getParameters()
    {
        return [
            'base' => 'https://gitlab.com',
            'appId' => 'GITLAB_APP_ID',
            'secret' => 'GITLAB_CLIENT_SECRET',
        ];
    }
}
