<?php
namespace Avris\Micrus\Social\Source\Gitlab;

use Avris\Micrus\Controller\Http\Request;
use Avris\Micrus\Exception\UnauthorisedException;
use Avris\Micrus\Social\Source\Handler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class GitlabHandler extends Handler
{
    const SESSION_STATE = 'gitlab_state';

    /** @var Client */
    protected $client;

    /** @var string */
    protected $base;

    /** @var string */
    protected $appId;

    /** @var string */
    protected $secret;

    public function __construct($access)
    {
        $this->client = new Client();
        $this->base = $access['base'];
        $this->appId = $access['appId'];
        $this->secret = $access['secret'];
    }

    public function login(Request $request, $redirectUrl)
    {
        if ($request->getQuery('error')) {
            throw new UnauthorisedException(sprintf('Access denied by user, error: %s', $request->getQuery('error')));
        }

        if (!$request->getQuery('code')) {
            $state = hash('sha256', microtime(true) . rand() . $request->getIp());

            $request->getSession()->set(self::SESSION_STATE, $state);

            $params = [
                'client_id' => $this->appId,
                'redirect_uri' => $redirectUrl,
                'response_type' => 'code',
                'state' => $state,
            ];

            return $this->base . '/oauth/authorize' . '?' . http_build_query($params);
        }

        $queryState = $request->getQuery('state');
        $sessionState = $request->getSession(self::SESSION_STATE);
        if (!$queryState || $queryState !== $sessionState) {
            throw new UnauthorisedException('Invalid state');
        }

        $response = $this->client->request('POST', $this->base . '/oauth/token', [
            'json' => [
                'client_id' => $this->appId,
                'client_secret' => $this->secret,
                'redirect_uri' => $redirectUrl,
                'state' => $sessionState,
                'code' => $request->getQuery('code'),
                'grant_type' => 'authorization_code',
            ],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $response = json_decode($response->getBody(), true);

        if (!isset($response['access_token'])) {
            throw new UnauthorisedException('Invalid access token');
        }

        return $this->loadUserData($response['access_token']);
    }

    /**
     * @param string $accessToken
     * @return array
     */
    protected function loadUserData($accessToken)
    {
        $response = $this->client->request('GET', $this->base . '/api/v3/user?access_token=' . $accessToken);

        $response = json_decode($response->getBody(), true);

        $interestingKeys = ['id', 'name', 'username', 'avatar_url', 'web_url', 'email'];

        $userData = array_intersect_key($response, array_flip($interestingKeys));
        $userData['accessToken'] = $accessToken;

        return $userData;
    }

    public function revoke(array $payload)
    {
        return true;
    }

    public function update(array $payload)
    {
        return $this->loadUserData($payload['accessToken']);
    }
}