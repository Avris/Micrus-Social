<?php
namespace Avris\Micrus\Social\Source;

use Avris\Micrus\Controller\Http\Request;

abstract class Handler
{
    /**
     * @param string[] $access
     */
    abstract public function __construct($access);

    /**
     * @param Request $request
     * @param string $redirectUrl
     * @return array|string
     * @throws \Exception
     */
    abstract public function login(Request $request, $redirectUrl);

    /**
     * @param string[] $payload
     * @return bool
     */
    abstract public function revoke(array $payload);

    /**
     * @param array $payload
     * @return array
     */
    abstract public function update(array $payload);
}
