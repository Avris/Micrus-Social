<?php
namespace Avris\Micrus\Social\Source;

use Avris\Micrus\Model\User\UserInterface;

abstract class SocialSource
{
    // Overwrite those values in the subclasses
    protected static $name = '';
    protected static $icon = '';
    protected static $handlerClass = '';
    protected static $accountClass = '';

    /** @var array */
    protected $access;

    /**
     * @param array $access
     */
    public function __construct($access)
    {
        $this->access = $access;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return static::$name;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return static::$icon;
    }

    /**
     * @return Handler
     */
    public function buildHandler()
    {
        return new static::$handlerClass($this->access);
    }

    /**
     * @param UserInterface $user
     * @return Account
     */
    public function buildSocialAccount(UserInterface $user)
    {
        foreach ($user->getAuthenticators($this->getName()) as $auth) {
            return new static::$accountClass($auth->getPayload());
        }

        return null;
    }

    public function getParameters()
    {
        return [];
    }
}
