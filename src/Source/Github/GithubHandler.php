<?php
namespace Avris\Micrus\Social\Source\Github;

use Avris\Micrus\Controller\Http\Request;
use Avris\Micrus\Exception\UnauthorisedException;
use Avris\Micrus\Social\Source\Handler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class GithubHandler extends Handler
{
    const AUTHORIZE_URL = 'https://github.com/login/oauth/authorize';
    const TOKEN_URL = 'https://github.com/login/oauth/access_token';
    const API_URL_BASE = 'https://api.github.com';

    const SESSION_STATE = 'github_state';

    /** @var Client */
    protected $client;

    /** @var string */
    protected $clientId;

    /** @var string */
    protected $secret;

    public function __construct($access)
    {
        $this->client = new Client();
        $this->clientId = $access['clientId'];
        $this->secret = $access['secret'];
    }

    public function login(Request $request, $redirectUrl)
    {
        if (!$request->getQuery('code')) {
            $state = hash('sha256', microtime(true) . rand() . $request->getIp());

            $request->getSession()->set(self::SESSION_STATE, $state);

            $params = [
                'client_id' => $this->clientId,
                'redirect_uri' => $redirectUrl,
                'scope' => 'user, repo',
                'state' => $state,
            ];

            return self::AUTHORIZE_URL . '?' . http_build_query($params);
        }

        $queryState = $request->getQuery('state');
        $sessionState = $request->getSession(self::SESSION_STATE);
        if (!$queryState || $queryState !== $sessionState) {
            throw new UnauthorisedException('Invalid state');
        }

        $response = $this->client->request('POST', self::TOKEN_URL, [
            'json' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->secret,
                'redirect_uri' => $redirectUrl,
                'state' => $sessionState,
                'code' => $request->getQuery('code'),
            ],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $response = json_decode($response->getBody(), true);

        if (!isset($response['access_token'])) {
            throw new UnauthorisedException('Invalid access token');
        }

        return $this->loadUserData($response['access_token']);
    }

    public function update(array $payload)
    {
        return $this->loadUserData($payload['accessToken']);
    }

    public function revoke(array $payload)
    {
        $url = sprintf(
            '%s/applications/%s/tokens/%s',
            self::API_URL_BASE ,
            $this->clientId,
            $payload['accessToken']
        );

        try {
            $this->client->request('DELETE', $url, [
                'auth' => [ $this->clientId, $this->secret ],
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]);

            return true;
        } catch (BadResponseException $e) {
            return false;
        }
    }

    /**
     * @param string $accessToken
     * @return array
     */
    protected function loadUserData($accessToken)
    {
        $response = $this->client->request('GET', self::API_URL_BASE . '/user', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);

        $response = json_decode($response->getBody(), true);

        $interestingKeys = ['id', 'login', 'name', 'html_url', 'email', 'avatar_url'];

        $userData = array_intersect_key($response, array_flip($interestingKeys));
        $userData['accessToken'] = $accessToken;

        return $userData;
    }
}