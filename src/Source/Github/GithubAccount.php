<?php
namespace Avris\Micrus\Social\Source\Github;

use Avris\Micrus\Social\Source\Account;

class GithubAccount extends Account
{
    protected static $map = [
        'id' => 'id',
        'name' => 'name',
        'subname' => 'login',
        'email' => 'email',
        'avatar' => 'avatar_url',
        'link' => 'html_url',
    ];

    public function getAvatar($size = 36)
    {
        return $this->avatar . '&size=' . $size;
    }
}
