<?php
namespace Avris\Micrus\Social\Source\Github;

use Avris\Micrus\Social\Source\SocialSource;
use Avris\Micrus\Tool\Config\ParametersProvider;

class GithubSource extends SocialSource
{
    protected static $name = 'github';
    protected static $icon = 'fa fa-github';
    protected static $handlerClass = GithubHandler::class;
    protected static $accountClass = GithubAccount::class;

    public function getParameters()
    {
        return [
            'clientId' => 'GITHUB_CLIENT_ID',
            'secret' => 'GITHUB_CLIENT_SECRET',
        ];
    }
}
