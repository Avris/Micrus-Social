<?php
namespace Avris\Micrus\Social\Source\Facebook;

use Avris\Micrus\Social\Source\SocialSource;
use Avris\Micrus\Tool\Config\ParametersProvider;

class FacebookSource extends SocialSource
{
    protected static $name = 'facebook';
    protected static $icon = 'fa fa-facebook-official';
    protected static $handlerClass = FacebookHandler::class;
    protected static $accountClass = FacebookAccount::class;

    public function getParameters()
    {
        return [
            'appId' => 'FB_APP_ID',
            'secret' => 'FB_APP_SECRET',
        ];
    }
}
