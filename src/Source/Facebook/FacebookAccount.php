<?php
namespace Avris\Micrus\Social\Source\Facebook;

use Avris\Micrus\Social\Source\Account;

class FacebookAccount extends Account
{
    public function getLink()
    {
        return 'https://facebook.com/' . $this->id;
    }

    public function getAvatar($size = 36)
    {
        return 'https://graph.facebook.com/' . $this->id . '/picture?type=square&height=' . $size;
    }
}
