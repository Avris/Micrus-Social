<?php
namespace Avris\Micrus\Social\Source\Facebook;

use Avris\Micrus\Controller\Http\Request;
use Avris\Micrus\Exception\UnauthorisedException;
use Avris\Micrus\Model\User\AuthenticatorInterface;
use Avris\Micrus\Social\Source\Handler;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Facebook;

class FacebookHandler extends Handler
{
    /** @var Facebook */
    protected $fb;

    public function __construct($access)
    {
        $this->fb = new Facebook([
            'app_id'  => $access['appId'],
            'app_secret' => $access['secret'],
        ]);
    }

    public function login(Request $request, $redirectUrl)
    {
        if ($request->getQuery('error')) {
            throw new UnauthorisedException(sprintf('Access denied by user, error: %s', $request->getQuery('error')));
        }

        if (!$request->getQuery('code')) {
            return $this->fb->getRedirectLoginHelper()->getLoginUrl(
                $redirectUrl,
                ['email']
            );
        }

        $accessToken = $this->fb->getRedirectLoginHelper()->getAccessToken();

        if (!$accessToken) {
            throw new UnauthorisedException('Invalid access token');
        }

        $oAuth2Client = $this->fb->getOAuth2Client();

        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        $tokenMetadata->validateAppId((string)$this->fb->getApp()->getId());
        $tokenMetadata->validateExpiration();

        if (!$accessToken->isLongLived()) {
            $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
        }

        return $this->loadUserData((string) $accessToken);
    }

    public function update(array $payload)
    {
        try {
            return $this->loadUserData($payload['accessToken']);
        } catch (FacebookResponseException $e) {
            return false;
        }
    }

    public function revoke(array $payload)
    {
        $this->fb->setDefaultAccessToken($payload['accessToken']);

        try {
            $response = $this->fb->delete('/' . $payload['id'] . '/permissions');
            return $response->getDecodedBody()['success'];
        } catch (FacebookResponseException $e) {
            return false;
        }
    }

    /**
     * @param string $accessToken
     * @return array
     */
    protected function loadUserData($accessToken)
    {
        $userData = $this->fb->get('/me?fields=id,name,email', $accessToken)->getDecodedBody();
        $userData['accessToken'] = $accessToken;

        return $userData;
    }
}