<?php
namespace Avris\Micrus\Social\Source;

abstract class Account
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $name;

    /** @var string */
    protected $subname;

    /** @var string */
    protected $email;

    /** @var string */
    protected $link;

    /** @var string */
    protected $avatar;

    protected static $map = [
        'id' => 'id',
        'name' => 'name',
        'subname' => 'subname',
        'email' => 'email',
        'link' => 'link',
        'avatar' => 'avatar',
    ];

    public function __construct(array $payload)
    {
        foreach (static::$map as $accountKey => $payloadKey) {
            $this->$accountKey = isset($payload[$payloadKey]) ? $payload[$payloadKey] : null;
        }
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSubName()
    {
        return $this->subname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function getAvatar($size = 36)
    {
        return $this->avatar;
    }

    public function __toString()
    {
        $out = '';

        if ($this->getName()) {
            $out = $this->getName();
        }

        if ($this->getSubName()) {
            if ($out) {
                $out .= ' (' . $this->getSubName() . ')';
            } else {
                $out = $this->getSubName();
            }
        }

        if ($this->getEmail()) {
            $out = $out ? $out . PHP_EOL . $this->getEmail() : $this->getEmail();
        }

        return $out;
    }
}
