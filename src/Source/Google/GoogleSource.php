<?php
namespace Avris\Micrus\Social\Source\Google;

use Avris\Micrus\Social\Source\SocialSource;
use Avris\Micrus\Tool\Config\ParametersProvider;

class GoogleSource extends SocialSource
{
    protected static $name = 'google';
    protected static $icon = 'fa fa-google-plus-square';
    protected static $handlerClass = GoogleHandler::class;
    protected static $accountClass = GoogleAccount::class;

    public function getParameters()
    {
        return [
            'client_id' => 'CLIENT_ID',
            'client_secret' => 'CLIENT_SECRET',
            'application_name' => 'APPLICATION_NAME',
        ];
    }
}
