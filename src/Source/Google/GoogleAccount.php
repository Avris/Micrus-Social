<?php
namespace Avris\Micrus\Social\Source\Google;

use Avris\Micrus\Social\Source\Account;

class GoogleAccount extends Account
{
    public function getAvatar($size = 36)
    {
        return str_replace('?sz=50', '?sz=' . $size, $this->avatar);
    }
}
