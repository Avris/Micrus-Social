<?php
namespace Avris\Micrus\Social\Source\Google;

use Avris\Micrus\Controller\Http\Request;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Exception\UnauthorisedException;
use Avris\Micrus\Model\User\AuthenticatorInterface;
use Avris\Micrus\Social\Source\Handler;
use Google_Client;
use Google_Service_Plus;
use Google_Service_Plus_PersonEmails;

class GoogleHandler extends Handler
{
    /** @var Google_Client */
    protected $client;

    public function __construct($access)
    {
        $this->client = new Google_Client($access);
    }

    public function login(Request $request, $redirectUrl)
    {
        if ($request->getQuery('error')) {
            throw new UnauthorisedException(sprintf('Access denied by user, error: %s', $request->getQuery('error')));
        }

        $this->client->setAccessType('offline');
        $this->client->setScopes([Google_Service_Plus::USERINFO_EMAIL]);
        $this->client->setRedirectUri($redirectUrl);

        if (!$request->getQuery('code')) {
            return $this->client->createAuthUrl();
        }

        $this->client->authenticate($request->getQuery('code'));
        $this->client->verifyIdToken();

        return $this->loadUserData($this->client->getRefreshToken());
    }

    public function update(array $payload)
    {
        $this->client->fetchAccessTokenWithRefreshToken($payload['refreshToken']);

        return $this->loadUserData($payload['refreshToken']);
    }

    public function revoke(array $payload)
    {
        try {
            $this->client->fetchAccessTokenWithRefreshToken($payload['refreshToken']);

            return $this->client->revokeToken();
        } catch (\LogicException $e) {
            return false;
        }
    }

    /**
     * @return array
     */
    protected function loadUserData($refreshToken)
    {
        $me = (new Google_Service_Plus($this->client))->people->get('me');

        return [
            'id' => $me->getId(),
            'name' => $me->getDisplayName(),
            'email' => $this->getPrimaryEmail($me->getEmails()),
            'avatar' => $me->getImage()['url'],
            'link' => $me->getUrl(),
            'refreshToken' => $refreshToken,
        ];
    }

    /**
     * @param Google_Service_Plus_PersonEmails[] $emails
     * @return string
     * @throws InvalidArgumentException
     */
    protected function getPrimaryEmail($emails)
    {
        foreach ($emails as $email) {
            if ($email->getType() === 'account') {
                return $email->getValue();
            }
        }

        throw new InvalidArgumentException('Primary email not found');
    }
}
