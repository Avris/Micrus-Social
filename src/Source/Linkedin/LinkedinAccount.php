<?php
namespace Avris\Micrus\Social\Source\Linkedin;

use Avris\Micrus\Social\Source\Account;

class LinkedinAccount extends Account
{
    protected static $map = [
        'id' => 'id',
        'name' => 'formattedName',
        'email' => 'emailAddress',
        'avatar' => 'pictureUrl',
        'link' => 'publicProfileUrl',
    ];
}
