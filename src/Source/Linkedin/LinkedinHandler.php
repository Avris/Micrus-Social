<?php
namespace Avris\Micrus\Social\Source\Linkedin;

use Avris\Micrus\Controller\Http\Request;
use Avris\Micrus\Exception\UnauthorisedException;
use Avris\Micrus\Social\Source\Handler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class LinkedinHandler extends Handler
{
    const AUTHORIZE_URL = 'https://www.linkedin.com/oauth/v2/authorization';
    const TOKEN_URL = 'https://www.linkedin.com/oauth/v2/accessToken';
    const API_URL_BASE = 'https://api.linkedin.com/v1';
    const API_USER = '/people/~:(id,formatted-name,email-address,picture-url,public-profile-url)?format=json';

    const SESSION_STATE = 'linkedin_state';

    /** @var Client */
    protected $client;

    /** @var string */
    protected $clientId;

    /** @var string */
    protected $secret;

    public function __construct($access)
    {
        $this->client = new Client();
        $this->clientId = $access['clientId'];
        $this->secret = $access['secret'];
    }

    public function login(Request $request, $redirectUrl)
    {
        if ($request->getQuery('error')) {
            throw new UnauthorisedException(sprintf('Access denied by user, error: %s', $request->getQuery('error')));
        }

        if (!$request->getQuery('code')) {
            $state = hash('sha256', microtime(true) . rand() . $request->getIp());

            $request->getSession()->set(self::SESSION_STATE, $state);

            $params = [
                'response_type' => 'code',
                'client_id' => $this->clientId,
                'redirect_uri' => $redirectUrl,
                'state' => $state,
            ];

            return self::AUTHORIZE_URL . '?' . http_build_query($params);
        }

        $queryState = $request->getQuery('state');
        $sessionState = $request->getSession(self::SESSION_STATE);
        if (!$queryState || $queryState !== $sessionState) {
            throw new UnauthorisedException('Invalid state');
        }

        $response = $this->client->request('POST', self::TOKEN_URL, [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => $this->clientId,
                'client_secret' => $this->secret,
                'redirect_uri' => $redirectUrl,
                'state' => $sessionState,
                'code' => $request->getQuery('code'),
            ],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $response = json_decode($response->getBody(), true);

        if (!isset($response['access_token'])) {
            throw new UnauthorisedException('Invalid access token');
        }

        return $this->loadUserData($response['access_token']);
    }

    public function update(array $payload)
    {
        return $this->loadUserData($payload['accessToken']);
    }

    public function revoke(array $payload)
    {
        return true;
    }

    /**
     * @param string $accessToken
     * @return array
     */
    protected function loadUserData($accessToken)
    {
        $response = $this->client->request('GET', self::API_URL_BASE . self::API_USER, [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);

        $userData = json_decode($response->getBody(), true);
        $userData['accessToken'] = $accessToken;

        return $userData;
    }
}