<?php
namespace Avris\Micrus\Social\Source\Linkedin;

use Avris\Micrus\Social\Source\SocialSource;
use Avris\Micrus\Tool\Config\ParametersProvider;

class LinkedinSource extends SocialSource
{
    protected static $name = 'linkedin';
    protected static $icon = 'fa fa-linkedin-square';
    protected static $handlerClass = LinkedinHandler::class;
    protected static $accountClass = LinkedinAccount::class;

    public function getParameters()
    {
        return [
            'clientId' => 'LINKEDIN_CLIENT_ID',
            'secret' => 'LINKEDIN_CLIENT_SECRET',
        ];
    }
}
