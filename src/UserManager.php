<?php
namespace Avris\Micrus\Social;

use App\Model\Authenticator;
use App\Model\User;
use Avris\Micrus\Bootstrap\EventDispatcherInterface;
use Avris\Micrus\Controller\Http\CookieBag;
use Avris\Micrus\Controller\Http\RequestInterface;
use Avris\Micrus\Controller\Routing\Service\Router;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Social\Event\UserLoggedInEvent;
use Avris\Micrus\Social\Event\UserSocialLoggedInEvent;
use Avris\Micrus\Mailer\Mailer;
use Avris\Micrus\Model\User\AuthenticatorInterface;
use Avris\Micrus\Social\Source\SocialSource;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\CryptInterface;
use Avris\Micrus\Tool\Security\SecurityManager;
use Doctrine\ORM\EntityManager;

class UserManager implements UserManagerInterface
{
    const SOCIAL_LOGIN_PENDING = 'socialLoginPending';
    const AUTHENTICATOR_EMAIL_CONFIRM = 'emailConfirm';
    const AUTHENTICATOR_EMAIL_MAGIC = 'emailMagicLink';

    /** @var EntityManager */
    protected $em;

    /** @var SecurityManager */
    protected $sm;

    /** @var CryptInterface */
    protected $crypt;

    /** @var RequestInterface */
    protected $request;

    /** @var FlashBag */
    protected $flashBag;

    /** @var MailManagerInterface */
    protected $mailManager;

    /** @var Mailer */
    protected $mailer;

    /** @var Router */
    protected $router;

    /** @var string */
    protected $afterLogin;

    /** @var EventDispatcherInterface */
    protected $dispatcher;

    public function __construct(
        EntityManager $em,
        SecurityManager $sm,
        CryptInterface $crypt,
        RequestInterface $request,
        FlashBag $flashBag,
        MailManagerInterface $mailManager,
        Router $router,
        EventDispatcherInterface $dispatcher,
        $afterLogin = null
    ) {
        $this->em = $em;
        $this->sm = $sm;
        $this->crypt = $crypt;
        $this->request = $request;
        $this->flashBag = $flashBag;
        $this->mailManager = $mailManager;
        $this->router = $router;
        $this->dispatcher = $dispatcher;
        $this->afterLogin = $afterLogin ?: Router::DEFAULT_ROUTE;
    }

    public function login(User $user, CookieBag $responseCookies)
    {
        if (!$user->getEmail()) {
            $this->flashBag->add('danger', l('social.login.emailNotConfirmed'));
            return 'userLogin';
        }

        if (!$user->isActive()) {
            $this->flashBag->add('danger', l('social.login.inactive'));
            return 'userLogin';
        }

        $this->sm->generateCookieToken($user, $responseCookies);

        $event = new UserLoggedInEvent($user);
        $this->dispatcher->trigger($event);
        $user = $event->getUser();
        if (!$user) {
            return 'userLogin';
        }

        $this->em->persist($user);
        $this->em->flush();
        $this->sm->login($user);

        return $this->afterLogin;
    }

    public function register(User $user, $password)
    {
        $user->createAuthenticator(
            SecurityManager::AUTHENTICATOR_PASSWORD,
            $this->crypt->hash($password)
        );

        $this->flashBag->add('success', l('user.register.success'));

        $this->sendConfirmEmail($user);

        return 'userLogin';
    }

    public function sendConfirmEmail(User $user, $oldEmail = null)
    {
        $token = $this->crypt->generateSecret();
        $user->createAuthenticator(
            static::AUTHENTICATOR_EMAIL_CONFIRM,
            ['email' => $user->getEmail(), 'token' => $token]
        );

        $this->mailManager->send('confirmEmail', $user, [
            'confirmLink' => $this->router->getUrl('userConfirmEmail', ['token' => $token], true)
        ]);

        $this->flashBag->add('success', l('user.confirmEmail.sent', [
            'email' => $user->getEmail(),
        ]));

        $user->setEmail($oldEmail);
        $this->em->persist($user);
        $this->em->flush();
    }

    public function createMagicLink(User $user = null)
    {
        if (!$user || !$user->isActive() || !$user->getEmail()) {
            // we lie, in order not to reveal, if given user exists or not
            $this->flashBag->add('success', l('user.magicLink.sent'));
            return 'userLogin';
        }

        $token = $this->crypt->generateSecret();
        $user->createAuthenticator(
            static::AUTHENTICATOR_EMAIL_MAGIC,
            ['email' => $user->getEmail(), 'token' => $token],
            new \DateTime('+10 minutes')
        );

        $this->mailManager->send('magicLink', $user, [
            'magicLink' => $this->router->getUrl('userMagicLink', ['token' => $token], true)
        ]);

        $this->flashBag->add('success', l('user.magicLink.sent'));

        $this->em->persist($user);
        $this->em->flush();

        return 'userLogin';
    }

    public function updatePassword(User $user, $newPassword)
    {
        foreach ($user->getAuthenticators(SecurityManager::AUTHENTICATOR_PASSWORD) as $auth) {
            $auth->invalidate();
        }

        $auth = $user->createAuthenticator(
            SecurityManager::AUTHENTICATOR_PASSWORD,
            $this->crypt->hash($newPassword)
        );

        return $auth;
    }

    public function loginSocial(SocialSource $source, $payload, CookieBag $responseCookies)
    {
        $userFromSession = $this->sm->getUser();
        $userFromAuth = $this->em->getRepository('Authenticator')->findUserBySocialAuth($source->getName(), $payload['id']);

        if ($userFromSession && $userFromAuth && $userFromSession != $userFromAuth) {
            $this->flashBag->add('danger', l('social.connect.duplicate', [
                'source' => $source,
                'user' => $userFromAuth,
            ]));

            return 'userLogin';
        }

        /** @var User $user */
        $user = $userFromSession ?: $userFromAuth;

        $event = new UserSocialLoggedInEvent($source, $payload, $user);
        $this->dispatcher->trigger($event);
        $user = $event->getUser();

        if (!$user) {
            $this->request->getSession()->set(static::SOCIAL_LOGIN_PENDING, [$source->getName(), $payload]);

            return RouterInterface::DEFAULT_ROUTE;
        }

        if (!$user->getAuthenticators($source->getName())->count()) {
            $user->createAuthenticator($source->getName(), $payload);
        }

        $this->login($user, $responseCookies);

        return $userFromSession ? 'userAccount' : $this->afterLogin;
    }

    public function loginSocialFail(SocialLoginException $e)
    {
        $this->flashBag->add('danger', l('social.connect.failed', ['source' => $e->getSource()]));

        return 'userLogin';
    }

    public function revokeSocial(AuthenticatorInterface $auth)
    {
        /** @var Authenticator $auth */
        $auth->invalidate();
        $this->em->persist($auth);
        $this->em->flush();
    }

    public function updateSocial(AuthenticatorInterface $auth, array $old, array $new)
    {
        /** @var Authenticator $auth */
        $auth->invalidate();
        $newAuth = $auth->getUser()->createAuthenticator($auth->getType(), $new);

        $this->em->persist($auth);
        $this->em->persist($newAuth);
        $this->em->flush();
    }
}
