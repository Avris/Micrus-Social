<?php
namespace Avris\Micrus\Social;

use Avris\Micrus\Doctrine\Doctrine;
use Avris\Micrus\Model\User\UserProviderInterface;

class EmailOrUsernameUserProvider implements UserProviderInterface
{
    /** @var Doctrine */
    protected $orm;

    public function __construct(Doctrine $orm)
    {
        $this->orm = $orm;
    }

    public function getUser($identifier)
    {
        return $this->orm->findOneBy('User', strpos($identifier, '@') === false ? 'username' : 'email', $identifier);
    }
}
