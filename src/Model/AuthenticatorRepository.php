<?php
namespace Avris\Micrus\Social\Model;

use App\Model\Authenticator;
use App\Model\User;
use Doctrine\ORM\EntityRepository;

class AuthenticatorRepository extends EntityRepository
{
    /**
     * @param string $source
     * @param string $id
     * @return User|null
     */
    public function findUserBySocialAuth($source, $id)
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.type = :type')
            ->andWhere('a.payload LIKE :id')
            ->andWhere('a.validUntil IS NULL OR a.validUntil >= :validUntil')
            ->setParameter('type', $source)
            ->setParameter('id', '%' . $id . '%')
            ->setParameter('validUntil', new \DateTime());

        /** @var Authenticator $auth */
        foreach ($qb->getQuery()->getResult() as $auth) {
            if ($auth->getPayload()['id'] === $id) {
                return $auth->getUser();
            }
        }

        return null;
    }

    /**
     * @param string $token
     * @param string $type
     * @return Authenticator|null
     */
    public function findByToken($type, $token)
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.type = :type')
            ->andWhere('a.payload LIKE :token')
            ->andWhere('a.validUntil IS NULL OR a.validUntil >= :validUntil')
            ->setParameter('type', $type)
            ->setParameter('token', '%' . $token . '%')
            ->setParameter('validUntil', new \DateTime());

        /** @var Authenticator $auth */
        foreach ($qb->getQuery()->getResult() as $auth) {
            if ($auth->getPayload()['token'] === $token) {
                return $auth;
            }
        }

        return null;
    }
}
