<?php
namespace Avris\Micrus\Social\Model;

use Avris\Micrus\Model\User\AuthenticatorInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 **/
abstract class BaseAuthenticator implements AuthenticatorInterface
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=36, options={"fixed" = true})
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(type="json_array")
     */
    protected $payload;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $validUntil;

    /**
     * @var BaseUser
     * @ORM\ManyToOne(targetEntity="User", inversedBy="authenticators")
     **/
    protected $user;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return array
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     * @return $this
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return BaseUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param BaseUser $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * @param \DateTime $validUntil
     * @return $this
     */
    public function setValidUntil(\DateTime $validUntil = null)
    {
        $this->validUntil = $validUntil;
        return $this;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->getValidUntil() === null || $this->getValidUntil() >= new \DateTime();
    }

    /**
     * @return $this
     */
    public function invalidate()
    {
        return $this->setValidUntil(new \DateTime('0000-00-00'));
    }

    /**
     * @param string $field
     * @return string
     */
    public function get($field)
    {
        return isset($this->payload[$field])
            ? $this->payload[$field]
            : null;
    }

    /**
     * @param string $field
     * @param string $value
     * @return $this
     */
    public function set($field, $value)
    {
        $this->payload[$field] = $value;

        return $this;
    }
}
