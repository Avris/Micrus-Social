<?php
namespace Avris\Micrus\Social\Model;

use Avris\Micrus\Mailer\Mail\AddressInterface;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Tool\Security\SecurityManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 **/
abstract class BaseUser implements UserInterface, AddressInterface
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=36, options={"fixed" = true})
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true, nullable=true, length=160)
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $role;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var string
     * @ORM\Column(type="string")
     **/
    protected $avatar;

    /**
     * @var BaseAuthenticator[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Authenticator", mappedBy="user", cascade={"persist"})
     **/
    protected $authenticators;

    public function __construct($email = null)
    {
        $this->email = $email;
        $this->role = self::ROLE_USER;
        $this->authenticators = new ArrayCollection();
        $this->active = true;
        $this->createdAt = new \DateTime();
    }

    public function getIdentifier()
    {
        return $this->getEmail();
    }

    /**
     * @param string|null $type
     * @return BaseAuthenticator[]|ArrayCollection
     */
    public function getAuthenticators($type = null)
    {
        return $this->authenticators->filter(function (BaseAuthenticator $auth) use ($type) {
            $typeValid = $type ? $auth->getType() === $type : true;
            return $auth->isValid() && $typeValid;
        });
    }

    public function createAuthenticator($type, $payload, \DateTime $validUntil = null)
    {
        $auth = new \App\Model\Authenticator();
        $auth->setType($type);
        $auth->setPayload($payload);
        $auth->setUser($this);
        $auth->setValidUntil($validUntil);

        $this->authenticators[] = $auth;

        return $auth;
    }

    public function hasSetupPassword()
    {
        return $this->getAuthenticators(SecurityManager::AUTHENTICATOR_PASSWORD)->count() > 0;
    }

    /**
     * @return string[]
     */
    public function getRoles()
    {
        return [$this->getRole()];
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email ? strtolower(trim($email)) : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    public function __toString()
    {
        return (string) $this->getIdentifier();
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return $this
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
