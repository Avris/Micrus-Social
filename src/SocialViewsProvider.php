<?php
namespace Avris\Micrus\Social;

use Avris\Micrus\View\TemplateDirsProvider;

class SocialViewsProvider implements TemplateDirsProvider
{
    /**
     * @return array
     */
    public function getTemplateDirs()
    {
        return __DIR__ . '/View';
    }
}
