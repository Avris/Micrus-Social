<?php
namespace Avris\Micrus\Social;

use Avris\Micrus\Social\Avatar\AvatarService;
use Avris\Micrus\Social\Form\AccountForm;
use Avris\Micrus\Social\Form\LoginForm;
use Avris\Micrus\Social\Form\MagicLinkForm;
use Avris\Micrus\Social\Form\PasswordForm;
use Avris\Micrus\Social\Form\RegisterForm;
use Avris\Micrus\Social\Form\SocialPendingForm;
use App\Model\User;
use Avris\Micrus\Bootstrap\ContainerInterface;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Controller\Http\RedirectResponse;
use Avris\Micrus\Controller\Http\ResponseInterface;
use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Exception\UnauthorisedException;
use Avris\Micrus\Forms\Form;
use Avris\Micrus\Social\Model\AuthenticatorRepository;
use Avris\Micrus\Annotations as M;
use Doctrine\ORM\EntityRepository;

class SocialController extends Controller
{
    const FORM_REGISTER = RegisterForm::class;
    const FORM_SOCIAL_PENDING = SocialPendingForm::class;
    const FORM_LOGIN = LoginForm::class;
    const FORM_MAGIC_LINK = MagicLinkForm::class;
    const FORM_ACCOUNT = AccountForm::class;
    const FORM_PASSWORD = PasswordForm::class;

    /** @var UserManager */
    protected $um;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->um = $this->get('userManager');
    }

    /**
     * @M\Route("/login")
     */
    public function loginAction()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        $loginForm = $this->getForm(static::FORM_LOGIN);
        $loginForm->bind($this->getData());
        if ($loginForm->isValid()) {
            return $this->redirectAfterLogin(
                $this->um->login($loginForm->getUser(), $this->responseCookies)
            );
        }

        $registerForm = $this->getForm(static::FORM_REGISTER, new User());
        $registerForm->bind($this->getData());
        if ($registerForm->isValid()) {
            /** @var User $user */
            $user = $registerForm->getObject();
            $this->downloadAndSetAvatar($user, AvatarService::GRAVATAR);

            return $this->redirectAfterLogin(
                $this->um->register($user, $registerForm->getPassword())
            );
        }

        $magicLinkForm = $this->getForm(static::FORM_MAGIC_LINK);
        $magicLinkForm->bind($this->getData());
        if ($magicLinkForm->isValid()) {
            return $this->redirectToRoute(
                $this->um->createMagicLink($magicLinkForm->getUser())
            );
        }

        return $this->render([
            'loginForm' => $loginForm,
            'registerForm' => $registerForm,
            'magicLinkForm' => $magicLinkForm,
            'boundForm' => $this->determineBoundForm([$loginForm, $magicLinkForm, $registerForm]),
            'socialSources' => $this->get('socialManager')->getSources(),
        ]);
    }

    /**
     * @param Form[] $forms
     * @return string
     */
    protected function determineBoundForm(array $forms)
    {
        foreach ($forms as $form) {
            if ($form->isBound()) {
                return $form->getName();
            }
        }

        return end($forms)->getName();
    }

    protected function redirectAfterLogin($fallback = 'home')
    {
        $after = $this->getRequest()->getQuery('url');

        $url = $after
            ? $this->getRouter()->prependToUrl($after)
            : $this->generateUrl($fallback);

        return $this->redirect($url);
    }

    /**
     * @M\Route("/confirm-email/{token}")
     *
     * @param string $token
     * @return ResponseInterface
     */
    public function confirmEmailAction($token)
    {
        /** @var AuthenticatorRepository $repo */
        $repo = $this->getEm()->getRepository('Authenticator');

        $auth = $repo->findByToken(UserManager::AUTHENTICATOR_EMAIL_CONFIRM, $token);
        if (!$auth) {
            $this->addFlash('danger', l('user.confirmEmail.invalid'));
            return $this->redirectToRoute('userLogin');
        }

        $email = $auth->getPayload()['email'];

        if ($conflictUser = $this->getEm()->getRepository('User')->findOneBy(['email' => $email])) {
            $this->addFlash('danger', l('user.confirmEmail.duplicate', ['user' => $email]));
            return $this->redirectToRoute('home');
        }

        $auth->invalidate();
        $auth->getUser()->setEmail($email);
        $this->getSession()->delete(UserManager::SOCIAL_LOGIN_PENDING);

        $this->addFlash('success', l('user.confirmEmail.success'));

        return $this->redirectAfterLogin(
            $this->um->login($auth->getUser(), $this->responseCookies)
        );
    }

    /**
     * @M\Route("/magic-link/{token}")
     * @param string $token
     * @return ResponseInterface
     * @throws UnauthorisedException
     */
    public function magicLinkAction($token)
    {
        /** @var AuthenticatorRepository $repo */
        $repo = $this->getEm()->getRepository('Authenticator');

        $auth = $repo->findByToken(UserManager::AUTHENTICATOR_EMAIL_MAGIC, $token);
        if (!$auth) {
            $this->addFlash('danger', l('user.magicLink.invalid'));
            return $this->redirectToRoute('userLogin');
        }

        if ($auth->getPayload()['email'] !== $auth->getUser()->getEmail()) {
            throw new UnauthorisedException;
        }

        $auth->invalidate();

        return $this->redirectAfterLogin(
            $this->um->login($auth->getUser(), $this->responseCookies)
        );
    }

    /**
     * @M\Route("/login/social/finish", name="socialLoginPending")
     */
    public function socialLoginPendingAction()
    {
        if (!$this->getSession()->has(UserManager::SOCIAL_LOGIN_PENDING)) {
            return $this->redirectToRoute('home');
        }

        list($source, $payload) = $this->getSession()->get(UserManager::SOCIAL_LOGIN_PENDING);

        if ($this->getQuery()->has('revoke')) {
            return $this->socialLoginPendingRevoke($source, $payload);
        }

        $repo = $this->getEm()->getRepository('User');
        if ($userWithConflictingEmail = $this->checkSocialLoginPendingConflict($payload, $repo)) {
            $this->addFlash('danger', l('social.connect.emailConflict', [
                'email' => $userWithConflictingEmail->getEmail(),
                'user' => $userWithConflictingEmail->getIdentifier(),
                'source' => $source,
            ]));

            return $this->socialLoginPendingRevoke($source, $payload);
        }

        $emailConfirmed = isset($payload['email']);

        $user = new User();
        if ($emailConfirmed) {
            $user->setEmail($payload['email']);
        }

        $form = $this->getForm(static::FORM_SOCIAL_PENDING, $user, [
            'emailConfirmed' => $emailConfirmed,
        ]);
        $form->bind($this->getData());
        if ($form->isValid()) {
            return $this->redirectAfterLogin(
                $this->handleSocialLoginPendingForm($form, $source, $payload, $emailConfirmed)
            );
        }

        return $this->render([
            'hideMenu' => true,
            'form' => $form,
            'emailConfirmed' => isset($payload['email']),
            'source' => $source,
        ]);
    }

    protected function handleSocialLoginPendingForm(SocialPendingForm $form, $source, $payload, $emailConfirmed)
    {
        /** @var User $user */
        $user = $form->getObject();

        if ($form->getPassword()) {
            $this->um->updatePassword($user, $form->getPassword());
        }

        if (!$user->getAuthenticators($source)->count()) {
            $user->createAuthenticator($source, $payload);
        }

        $this->downloadAndSetAvatar($user, $source);

        if ($emailConfirmed) {
            $this->downloadAndSetAvatar($user, $source);
            $this->getSession()->delete(UserManager::SOCIAL_LOGIN_PENDING);
            $this->um->login($user, $this->responseCookies);

            return 'home';
        }

        $this->um->sendConfirmEmail($user);

        return 'socialLoginPending';
    }

    /**
     * @param array $payload
     * @param EntityRepository $repo
     * @return User|bool
     */
    protected function checkSocialLoginPendingConflict(array $payload, EntityRepository $repo)
    {
        if (!isset($payload['email'])) {
            return false;
        }

        return $repo->findOneBy(['email' => $payload['email']]) ?: false;
    }

    /**
     * @param $source
     * @param $payload
     * @return RedirectResponse
     */
    protected function socialLoginPendingRevoke($source, $payload)
    {
        /** @var SocialManager $socialManager */
        $socialManager = $this->get('socialManager');
        $socialManager->getSource($source)->buildHandler()->revoke($payload);
        $this->getSession()->delete(UserManager::SOCIAL_LOGIN_PENDING);

        return $this->redirectToRoute('home');
    }

    /**
     * @M\Route("/account")
     * @M\Secure
     */
    public function accountAction()
    {
        /** @var User $user */
        $user = $this->getUser();
        $oldEmail = $user->getEmail();

        $accountForm = $this->getForm(static::FORM_ACCOUNT, $user);
        $accountForm->bind($this->getData());
        if ($accountForm->isValid()) {
            if ($user->getEmail() !== $oldEmail) {
                $this->um->sendConfirmEmail($user, $oldEmail);
            }

            $user->setEmail($oldEmail);
            $this->getEm()->persist($user);
            $this->getEm()->flush();

            return $this->redirectToRoute('userAccount');
        }

        $passwordForm = $this->getForm(static::FORM_PASSWORD, $user);
        $passwordForm->bind($this->getData());
        if ($passwordForm->isValid()) {
            $this->um->updatePassword($user, $passwordForm->getPassword());
            $this->addFlash('success', l('user.account.changePassword.success'));

            $this->getEm()->persist($user);
            $this->getEm()->flush();

            return $this->redirectToRoute('userAccount');
        }

        return $this->render([
            'useUsername' => (bool) $this->get('config.?social.?useUsername'),
            'accountForm' => $accountForm,
            'passwordForm' => $passwordForm,
            'socialSources' => $this->get('socialManager')->getSources(),
            'gravatar' => $this->get('avatar')->getGravatar($user),
        ]);
    }

    /**
     * @M\Route("/account/updateAvatar/{source}")
     * @M\Secure
     */
    public function updateAvatarAction($source)
    {
        $this->downloadAndSetAvatar($this->getUser(), $source);
        $this->getEm()->persist($this->getUser());
        $this->getEm()->flush();

        $this->addFlash('success', l('user.account.updateAvatar.success'));

        return $this->redirectToRoute('userAccount');
    }

    /**
     * @param User $user
     * @param string $source
     */
    protected function downloadAndSetAvatar(User $user, $source)
    {
        /** @var UploadedFile $file */
        $file = $this->get('avatar')->downloadAvatar($user, $source);
        $user->setAvatar($file->moveToRandom($this->get('picDir')));
    }

    /**
     * @param $class
     * @param User|null $object
     * @param array $options
     * @return Form
     */
    protected function getForm($class, $object = null, $options = [])
    {
        return new $class($object, $this->container, $options);
    }
}
