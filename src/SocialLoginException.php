<?php
namespace Avris\Micrus\Social;

use Avris\Micrus\Social\Source\SocialSource;

class SocialLoginException extends \Exception
{
    /** @var SocialSource */
    protected $source;

    public function __construct(SocialSource $source, \Exception $previous)
    {
        $this->source = $source;
        parent::__construct('', 0, $previous);
    }

    /**
     * @return SocialSource
     */
    public function getSource()
    {
        return $this->source;
    }

}
