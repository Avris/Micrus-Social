<?php
namespace Avris\Micrus\Social;

use Avris\Micrus\Controller\Http\CookieBag;
use Avris\Micrus\Model\User\AuthenticatorInterface;
use Avris\Micrus\Social\Source\SocialSource;

interface UserManagerInterface
{
    /**
     * @param SocialSource $source
     * @param array $payload
     * @param CookieBag $responseCookies
     * @return null|string name of a route to redirect to
     */
    public function loginSocial(SocialSource $source, $payload, CookieBag $responseCookies);

    /**
     * @param SocialLoginException $e
     * @return string|null   name of a route to redirect to
     */
    public function loginSocialFail(SocialLoginException $e);

    /**
     * @param AuthenticatorInterface $auth
     * @param array $old
     * @param array $new
     */
    public function updateSocial(AuthenticatorInterface $auth, array $old, array $new);

    /**
     * @param AuthenticatorInterface $auth
     */
    public function revokeSocial(AuthenticatorInterface $auth);
}
