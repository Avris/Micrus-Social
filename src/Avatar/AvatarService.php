<?php
namespace Avris\Micrus\Social\Avatar;

use App\Model\User;
use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Exception\NotFoundException;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Social\SocialManager;
use Avris\Micrus\Tool\FileDownloader;

class AvatarService
{
    const GRAVATAR = 'gravatar';

    /** @var SocialManager */
    protected $socialManager;

    /** @var FileDownloader */
    protected $fileDownloader;

    /** @var AvatarGeneratorService */
    protected $avatarGenerator;

    /** @var int */
    protected $size;

    /**
     * @param SocialManager $socialManager
     * @param FileDownloader $fileDownloader
     * @param AvatarGeneratorService $avatarGenerator
     * @param int $size
     */
    public function __construct(
        SocialManager $socialManager,
        FileDownloader $fileDownloader,
        AvatarGeneratorService $avatarGenerator,
        $size = 128
    ) {
        $this->socialManager = $socialManager;
        $this->fileDownloader = $fileDownloader;
        $this->avatarGenerator = $avatarGenerator;
        $this->size = $size;
    }

    public function getGravatar(User $user)
    {
        return sprintf(
            'https://www.gravatar.com/avatar/%s.jpg?d=%s&s=%s',
            md5(strtolower(trim($user->getEmail()))),
            'identicon',
            $this->size
        );
    }

    /**
     * @param User $user
     * @param string $source
     * @return UploadedFile|bool
     * @throws NotFoundException
     */
    public function downloadAvatar(User $user, $source)
    {
        $url = $source === self::GRAVATAR ? $this->getGravatar($user) : $this->getAvatarUrlFromSocial($user, $source);

        $file = substr($url, 0, 5) === 'data:'
            ? $this->decodeData($url)
            : $this->fileDownloader->download($url, 'avatar.jpg');

        return $file ?: $this->avatarGenerator->generate($user->getIdentifier(), $this->size);
    }

    protected function getAvatarUrlFromSocial(UserInterface $user, $source)
    {
        return $this->socialManager->getSource($source)->buildSocialAccount($user)->getAvatar($this->size);
    }

    protected function decodeData($string)
    {
        if (!preg_match('#^data:(image/([A-Za-z0-9-]+));base64,([A-Za-z0-9+/=]+)$#', $string, $matches)) {
            return null;
        }

        $decoded = base64_decode($matches[3]);

        return $decoded ? new MemoryFile($decoded, $matches[1], $matches[2]) : null;
    }
}