<?php
namespace Avris\Micrus\Social\Avatar;

use Avris\Micrus\Controller\Http\MemoryFile;
use Imagine\Gd\Font;
use Imagine\Gd\Image;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Fill\Gradient\Vertical;
use Imagine\Image\ImageInterface;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;

class AvatarGeneratorService
{
    const MAX_SIZE = 360;

    /** @var Imagine */
    protected $imagine;

    /** @var RGB */
    protected $palette;

    /** @var string */
    protected $defaultFontFile;

    /**
     * @param string $fontFile
     */
    public function __construct($fontFile)
    {
        $this->imagine = new Imagine();
        $this->palette = new RGB();
        $this->defaultFontFile = $fontFile;
    }

    /**
     * @param string $text
     * @param int $size
     * @param float $textSize
     * @param float $posX
     * @param float $posY
     * @param string|null $fontFile
     * @return Image
     */
    public function generate($text, $size = self::MAX_SIZE, $textSize = 0.6, $posX = 0.5, $posY = 0.5, $fontFile = null)
    {
        if (!$text) {
            throw new \InvalidArgumentException('Text cannot be empty');
        }

        if ($size > self::MAX_SIZE) {
            throw new \InvalidArgumentException(sprintf('Max size is %sx%s', self::MAX_SIZE, self::MAX_SIZE));
        }

        $box = new Box($size, $size);
        $font = new Font(
            $fontFile ?: $this->defaultFontFile,
            $size * $textSize,
            $this->palette->color('fff')
        );

        $hash = md5($text);
        $color = $this->palette->color(substr($hash, 0, 6));

        $fill = new Vertical($box->getHeight(), $color->darken(127), $color);
        $letter = mb_strtoupper(mb_substr($text, 0, 1));

        $textImage = $this->imagine->create($box, $this->palette->color('000', 0));
        $textImage->draw()->text($letter, $font, new Point(0, 0));
        $textImage = $this->autoCrop($textImage);

        $image = $this->imagine->create($box);
        $image->fill($fill);
        $image->paste($textImage, $this->placeImage($image, $textImage, $posX, $posY));

        return new MemoryFile(
            $image->get('png'),
            'image/png',
            'png'
        );
    }

    /**
     * @param ImageInterface $image
     * @return ImageInterface
     */
    protected function autoCrop(ImageInterface $image)
    {
        $size = $image->getSize();
        $width = $size->getWidth();
        $height = $size->getHeight();

        $minX = $size->getWidth();
        $minY = $size->getHeight();
        $maxX = 0;
        $maxY = 0;

        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                if (!$image->getColorAt(new Point($x, $y))->getAlpha()) {
                    continue;
                }
                if ($x < $minX) {
                    $minX = $x;
                }
                if ($y < $minY) {
                    $minY = $y;
                }
                if ($x > $maxX) {
                    $maxX = $x;
                }
                if ($y > $maxY) {
                    $maxY = $y;
                }
            }
        }

        return $image->crop(new Point($minX, $minY), new Box($maxX - $minX + 1, $maxY - $minY + 1));
    }

    /**
     * @param ImageInterface $outer
     * @param ImageInterface $inner
     * @param int $posX
     * @param int $posY
     * @return Point
     */
    protected function placeImage(ImageInterface $outer, ImageInterface $inner, $posX, $posY)
    {
        return new Point(
            ($outer->getSize()->getWidth() - $inner->getSize()->getWidth()) * $posX,
            ($outer->getSize()->getHeight() - $inner->getSize()->getHeight()) * $posY
        );
    }
}
