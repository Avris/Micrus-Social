<?php
namespace Avris\Micrus\Social\Event;

use Avris\Micrus\Bootstrap\Event;
use Avris\Micrus\Mailer\Mail\AddressInterface;
use Avris\Micrus\Mailer\Mail\Mail;

class MailSentEvent extends Event
{
    /** @var string */
    private $templateName;

    /** @var AddressInterface */
    private $receiver;

    /** @var array */
    private $vars;

    /** @var bool */
    private $spool;

    /** @var Mail */
    private $mail;

    /**
     * @param string $templateName
     * @param AddressInterface $receiver
     * @param array $vars
     * @param bool $spool
     * @param Mail $mail
     */
    public function __construct($templateName, AddressInterface $receiver, array $vars, $spool, Mail $mail)
    {
        $this->templateName = $templateName;
        $this->receiver = $receiver;
        $this->vars = $vars;
        $this->spool = $spool;
        $this->mail = $mail;
    }

    /**
     * @return string
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * @return AddressInterface
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @return array
     */
    public function getVars()
    {
        return $this->vars;
    }

    /**
     * @return bool
     */
    public function isSpool()
    {
        return $this->spool;
    }

    /**
     * @param bool $spool
     * @return $this
     */
    public function setSpool($spool)
    {
        $this->spool = $spool;
        return $this;
    }

    /**
     * @return Mail
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param Mail $mail
     * @return $this
     */
    public function setMail(Mail $mail = null)
    {
        $this->mail = $mail;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mailSent';
    }
}