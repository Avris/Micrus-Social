<?php
namespace Avris\Micrus\Social\Event;

use Avris\Micrus\Bootstrap\Event;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Social\Source\SocialSource;

class UserSocialLoggedInEvent extends Event
{
    /** @var SocialSource */
    private $source;

    /** @var mixed */
    private $payload;

    /** @var UserInterface|null */
    private $user;

    /**
     * @param SocialSource $source
     * @param mixed $payload
     * @param UserInterface|null $user
     */
    public function __construct(SocialSource $source, $payload, UserInterface $user = null)
    {
        $this->user = $user;
        $this->source = $source;
        $this->payload = $payload;
    }

    /**
     * @return SocialSource
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return mixed
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @return UserInterface|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserInterface $user
     * @return $this
     */
    public function setUser(UserInterface $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'userSocialLoggedIn';
    }
}