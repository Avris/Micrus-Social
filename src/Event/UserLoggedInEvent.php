<?php
namespace Avris\Micrus\Social\Event;

use Avris\Micrus\Bootstrap\Event;
use Avris\Micrus\Model\User\UserInterface;

class UserLoggedInEvent extends Event
{
    /** @var UserInterface */
    private $user;

    /**
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserInterface $user
     * @return $this
     */
    public function setUser(UserInterface $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'userLoggedIn';
    }
}