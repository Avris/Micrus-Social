<?php
namespace Avris\Micrus\Social;

use Avris\Micrus\Bootstrap\ContainerInterface;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Exception\NotFoundException;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Social\Model\BaseUser;
use Avris\Micrus\Social\Source as Source;
use Avris\Micrus\Social\Source\SocialSource;
use Avris\Bag\Bag;
use Avris\Micrus\Tool\Config\ParametersProvider;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\SecurityManager;

class SocialManager extends Controller implements ParametersProvider
{
    const USER_MANAGER_SERVICE = 'userManager';

    /** @var array */
    protected $config;

    /** @var SocialSource[] */
    protected $sources = [];

    /** @var string */
    protected $base;

    /** @var string */
    protected $env;

    /** @var UserManagerInterface */
    protected $userManager;

    /**
     * @param ContainerInterface $container
     * @param UserManagerInterface $userManager
     * @param string $base
     * @param Bag $config
     * @param SocialSource[] $availableSources
     * @throws InvalidArgumentException
     */
    public function __construct(
        ContainerInterface $container,
        UserManagerInterface $userManager,
        $base,
        $env,
        Bag $config,
        array $availableSources = []
    ) {
        parent::__construct($container);

        $this->userManager = $userManager;
        $this->base = $base;
        $this->env = $env;

        $this->config = new Bag([
            'sources' => ['facebook', 'google', 'twitter'],
            'assertHasMethodLeft' => false,
        ]);
        $this->config->replace($config);

        $availableSourcesWithKeys = [];
        foreach ($availableSources as $availableSource) {
            $availableSourcesWithKeys[$availableSource->getName()] = $availableSource;
        }

        foreach ((array) $this->config->get('sources') as $enabledSource) {
            if (!isset($availableSourcesWithKeys[$enabledSource])) {
                throw new InvalidArgumentException(sprintf(
                    'Social login source "%s" is not supported',
                    $enabledSource
                ));
            }
            $this->sources[$enabledSource] = $availableSourcesWithKeys[$enabledSource];
        }
    }

    /**
     * @return Source\SocialSource[]
     */
    public function getSources()
    {
        return $this->sources;
    }

    /**
     * @param $sourceName
     * @param bool|false $checkPost
     * @return SocialSource
     * @throws NotFoundException
     */
    public function getSource($sourceName, $checkPost = false)
    {
        if (!isset($this->sources[$sourceName])
            || ($checkPost && $sourceName !== $this->getRequest()->getData('source'))) {
            throw new NotFoundException(sprintf('Social login source "%s" not found', $sourceName));
        }

        return $this->sources[$sourceName];
    }

    public function loginAction($sourceName)
    {
        $source = $this->getSource($sourceName);

        try {
            $redirectUrl = $this->base . $this->generateUrl('socialLogin', ['source' => $source]);

            $result = $source->buildHandler()->login($this->getRequest(), $redirectUrl);

            if (is_string($result)) {
                return $this->redirect($result);
            }

            $routeName = $this->userManager->loginSocial($source, $result, $this->responseCookies);
        } catch (\Exception $e) {
            if ($this->env === 'dev') {
                throw $e;
            }
            $this->get('logger')->error($e);
            $routeName = $this->userManager->loginSocialFail(new SocialLoginException($source, $e));
        }

        return $this->redirectToRoute($routeName ?: RouterInterface::DEFAULT_ROUTE);
    }

    public function updateAction($sourceName)
    {
        $source = $this->getSource($sourceName, true);

        $handler = $source->buildHandler();

        foreach ($this->getUser()->getAuthenticators($sourceName) as $auth) {
            $old = $auth->getPayload();
            $new = $handler->update($old);

            if ($old != $new) {
                $this->addFlash(FlashBag::INFO, l('social.updateData.noChanges', ['source' => $source]));
                continue;
            }

            $this->userManager->updateSocial($auth, $old, $new);
            $this->addFlash(FlashBag::SUCCESS, l('social.updateData.success', ['source' => $source]));
        }

        return $this->redirectToRoute('userAccount');
    }

    public function revokeAction($sourceName)
    {
        $source = $this->getSource($sourceName, true);

        if ($this->config['assertHasMethodLeft'] && !$this->assertHasMethodLeft($source)) {
            $this->addFlash(FlashBag::DANGER, l('social.disconnect.noMethodLeft', ['source' => $source]));

            return $this->redirectToRoute('userAccount');
        }

        $handler = $source->buildHandler();

        foreach ($this->getUser()->getAuthenticators($sourceName) as $auth) {
            $handler->revoke($auth->getPayload());
            $this->userManager->revokeSocial($auth);
            $this->addFlash(FlashBag::SUCCESS, l('social.disconnect.success', ['source' => $source]));
        }

        return $this->redirectToRoute('userAccount');
    }

    protected function assertHasMethodLeft(SocialSource $source)
    {
        $except = [
            $source->getName(),
            SecurityManager::AUTHENTICATOR_COOKIE,
            UserManager::AUTHENTICATOR_EMAIL_CONFIRM,
            UserManager::AUTHENTICATOR_EMAIL_MAGIC,
        ];

        foreach ($this->getUser()->getAuthenticators() as $auth) {
            if (!in_array($auth->getType(), $except)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param UserInterface $user
     * @return int
     */
    public function updateInfo(UserInterface $user)
    {
        $count = 0;

        $handlersCache = [];

        foreach ($user->getAuthenticators() as $auth) {
            if (!in_array($auth->getType(), $this->sources)) {
                continue;
            }

            $handler = isset($handlersCache[$auth->getType()])
                ? $handlersCache[$auth->getType()]
                : $handlersCache[$auth->getType()] = $this->getSource($auth->getType())->buildHandler();

            $old = $auth->getPayload();
            $new = $handler->update($old);

            if ($old != $new) {
                $this->userManager->updateSocial($auth, $old, $new);
                $count++;
            }
        }

        return $count;
    }

    /**
     * @param BaseUser $user
     * @param string $email
     * @return bool
     */
    public function hasConfirmedEmail(BaseUser $user, $email)
    {
        if ($user->getEmail() === $email) {
            return true;
        }

        foreach ($this->getSources() as $source) {
            $account = $source->buildSocialAccount($user);
            if ($account && $account->getEmail() === $email) {
                return true;
            }
        }

        return false;
    }

    /** @return array */
    public function getDefaultParameters()
    {
        $params = [];

        foreach ($this->sources as $source) {
            $params[$source->getName()] = $source->getParameters();
        }

        return [
            'social' => $params,
        ];
    }
}
